package com.funsocialapps.stitch;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.provider.OpenableColumns;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.util.Date;
import java.util.UUID;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

/**
 * Created by doug on 5/20/15.
 */
public class WebAPI {
    public static final String hostname = "api.funsocialapps.com";
    public static final String baseURL = "https://" + hostname;
    public static final String eventsURL = baseURL + "/events";

    public static final String staticWebSiteBaseURL = "http://stitch.funsocialapps.com";

    public static String getUniqueId() {
        return UUID.randomUUID().toString();
    }

    public static String getEventsNearURL(double latitude, double longitude) {
        return eventsURL + "?filter=near&latitude=" + latitude + "&longitude=" + longitude;
    }
    public static String getEventURL(String eventId) {
        return eventsURL + "/" + eventId;
    }
    public static final String getEventCapturesURL(String eventId) {
        return eventsURL + "/" + eventId + "/captures";
    }
    public static String getAssetMetadataURL(String eventId, String captureId) {
        return getEventCapturesURL(eventId) + "/" + captureId + "/assets";
    }

    private static String dataURLPath(String assetId) {
        return "/data/" + assetId;
    }

    public static String getDataURL(String assetId) {
        // Temporarily HTTP for gets to play nice with external components
        // that don't know about our self signed certificate.
        return baseURL + dataURLPath(assetId);
    }

    public static String postDataURL(String assetId) {
        return baseURL + dataURLPath(assetId);
    }

    public static String shareEventURL(String eventId) {
        return staticWebSiteBaseURL + "/event/?event=" + eventId;
    }

    public static String getString(String url) throws Exception {
        L.debug("GET: " + url);
        HttpURLConnection conn = openConnection(url);
        try {
            //d("Response Code: " + conn.getResponseCode());
            InputStream in = conn.getInputStream();
            try {
                String r = IOUtils.readUTF8Stream(in);
                L.debug("GET RETURNED: " + url);
                return r;
            } finally {
                in.close();
            }
        } finally {
            conn.disconnect();
        }
    }

    /**
     * Make an HTTPS request that returns a JSON response.
     * @param url
     * @return The JSONArray or JSONObject.
     * @throws Exception
     */
    public static Object getJSON(String url)throws Exception {
        String json = getString(url);
        JSONTokener tokener = new JSONTokener(json);
        return tokener.nextValue();
    }

    public static JSONArray getJSONArray(String url) throws Exception {
        return (JSONArray)getJSON(url);
    }

    public static JSONObject getJSONObject(String url) throws Exception {
        return (JSONObject)getJSON(url);
    }

    public static int sendJSONObject(String method, String url, JSONObject obj) throws Exception {
        return sendString(method, url, obj.toString(), "application/json");
    }

    public static int putJSONObject(String url, JSONObject obj) throws Exception {
        return sendJSONObject("PUT", url, obj);
    }

    public static int postJSONObject(String url, JSONObject obj) throws Exception {
        return sendJSONObject("POST", url, obj);
    }

    static private Charset UTF8 = Charset.forName("UTF-8"); // pre-API level 19 StandardCharsets.UTF_8

    private static String deviceId;

    public static void initDeviceId(String id) {
        deviceId = id;
    }

    static String getDeviceId() {
        return deviceId;
    }

    private static int sendString(String method, String url, String body, String contentType) throws Exception {
        HttpURLConnection conn = openConnection(url);
        try {
            conn.setRequestMethod(method);
            conn.setRequestProperty("Content-Type", contentType);
            conn.setDoOutput(true);
            byte[] utf8Body = body.getBytes(UTF8);
            ByteArrayInputStream in = new ByteArrayInputStream(utf8Body);
            IOUtils.copy(in, conn.getOutputStream());
            return conn.getResponseCode();
        } finally {
            conn.disconnect();
        }
    }

    private static int postString(String url, String body, String contentType) throws Exception {
        return sendString("POST", url, body, contentType);
    }

    private static int putString(String url, String body, String contentType) throws Exception {
        return sendString("PUT", url, body, contentType);
    }

    private static final int HTTPConnectTimeoutMiliseconds = 15 * 1000;
    private static final int HTTPReadTimeoutMiliseconds = 60 * 1000;

    public static HttpURLConnection openConnection(String url) throws IOException {
        HttpURLConnection conn = (HttpURLConnection)(new URL(url)).openConnection();
        conn.setConnectTimeout(HTTPConnectTimeoutMiliseconds);
        conn.setReadTimeout(HTTPReadTimeoutMiliseconds);
        conn.setRequestProperty("UserId", getDeviceId().toString());

        // TODO: Enable response caching for images like we do in iOS.

        if (!(conn instanceof HttpsURLConnection)) {
            L.error("Expected HttpsURLConnection but got HttpURLConnection. Everything should be HTTPS.");
        }

        return conn;
    }

    public static JSONObject bestVariantForTargetPixels(JSONObject capture, int targetPixels) throws JSONException {
        JSONArray variants = capture.getJSONArray("variants");

        JSONObject best = null;
        int bestPixels = 0;

        for(int i = 0; i < variants.length(); ++i) {
            JSONObject variant = variants.getJSONObject(i);
            String type = variant.getString("type");
            int width = variant.getInt("width");
            int height = variant.getInt("height");
            int pixels = width * height;

            if (!type.startsWith("image/")) {
                continue;
            }

            if (best == null || Math.abs(pixels - targetPixels) < Math.abs(bestPixels - targetPixels)) {
                best = variant;
                bestPixels = pixels;
            }
        }

        return best;
    }

    public static JSONObject bestVariantForSize(JSONObject capture, int width, int height) throws JSONException {
        return bestVariantForTargetPixels(capture, width * height);
    }

    public static JSONObject videoAssetForCapture(JSONObject capture) throws JSONException {
        JSONArray variants = capture.getJSONArray("variants");

        for(int i = 0; i < variants.length(); ++i) {
            JSONObject variant = variants.getJSONObject(i);
            String type = variant.getString("type");
            if (ContentType.isVideo(type)) {
                return variant;
            }
        }

        return null;
    }

    public static String postCapture(String eventId) throws Exception {
        String captureId = getUniqueId();
        JSONObject capture = new JSONObject();
        capture.put("id", captureId);
        capture.put("created", RFC3339.getDateFormat().format(new Date()));
        int responseCode = postJSONObject(getEventCapturesURL(eventId), capture);
        if (responseCode != HttpURLConnection.HTTP_CREATED) {
            throw new Exception("addCapture expected " + HttpURLConnection.HTTP_CREATED + " but got " + responseCode);
        }
        return captureId;
    }

    public static void postAssetMetadataToCapture(String eventId, String assetId, String captureId, long fileSize, int width, int height, String type) throws Exception {
        JSONObject asset = new JSONObject();
        asset.put("id", assetId);
        asset.put("type", type);
        if (width > 0) {
            asset.put("width", width);
        }
        if (height > 0) {
            asset.put("height", height);
        }
        asset.put("fileSize", fileSize);

        int responseCode = postJSONObject(getAssetMetadataURL(eventId, captureId), asset);
        if (responseCode != HttpURLConnection.HTTP_CREATED) {
            throw new Exception("addAssetToCapture expected " + HttpURLConnection.HTTP_CREATED + " but got " + responseCode);
        }
    }

    public static void postPhotoAssetMetadataToCapture(String eventId, File photo, String assetId, String captureId) throws Exception {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(photo.toString(), options);
        int width = options.outWidth;
        int height = options.outHeight;

        if (width == -1 || height == -1) {
            throw new Exception("addAssetToCapture error decoding bitmap to determine width and height");
        }

        L.debug("addAssetToCapture width=" + width + " height=" + height);

        long fileSize = photo.length();
        postAssetMetadataToCapture(eventId, assetId, captureId, fileSize, width, height, ContentType.JPEG);
    }

    // Returns asset Id on success.
    public static String postAssetData(Context context, Uri assetUri, String type) throws Exception {
        String assetId = getUniqueId();
        HttpURLConnection conn = WebAPI.openConnection(WebAPI.postDataURL(assetId));
        conn.setRequestProperty("Content-Type", type);
        conn.setDoOutput(true);

        InputStream assetInputStream = context.getContentResolver().openInputStream(assetUri);
        try {
            IOUtils.copy(assetInputStream, conn.getOutputStream());
        } finally {
            assetInputStream.close();
        }


        int responseCode = conn.getResponseCode();
        if (responseCode != HttpURLConnection.HTTP_CREATED) {
            throw new Exception("Unexpected HTTP response code putting asset data: " + responseCode);
        }

        L.debug("Successfully put asset Id " + assetId);

        return assetId;
    }

    public static void postPhoto(Context context, String eventId, Uri photo, File cacheDir) throws Exception {
        // Generate a thumbnail.
        File thumbnailJpeg = UI.scaledDownRotatedJPEGFile(context, photo, cacheDir, MAX_THUMBNAIL_EDGE_PIXELS, THUMBNAIL_JPEG_QUALITY);
        try {
            File decentJpeg = UI.scaledDownRotatedJPEGFile(context, photo, cacheDir, MAX_DECENT_EDGE_PIXELS, DECENT_JPEG_QUALITY);
            try {
                String thumbnailId = postAssetData(context, Uri.fromFile(thumbnailJpeg), ContentType.JPEG);
                String decentId = postAssetData(context, Uri.fromFile(decentJpeg), ContentType.JPEG);
                String captureId = postCapture(eventId).toString();
                postPhotoAssetMetadataToCapture(eventId, thumbnailJpeg, thumbnailId, captureId);
                postPhotoAssetMetadataToCapture(eventId, decentJpeg, decentId, captureId);
            } finally {
                decentJpeg.delete();
            }
        } finally {
            thumbnailJpeg.delete();
        }
    }

    private static long getFileSize(Context context, Uri uri) {

        if ("file".equals(uri.getScheme())) {
            File f = new File(uri.getPath());
            return f.length();
        }

        String[] projection = {OpenableColumns.SIZE};
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null && cursor.getCount() == 1) {
            try {
                cursor.moveToFirst();
                return cursor.getLong(0);
            } finally {
                cursor.close();
            }
        }

        throw new RuntimeException("Couldn't determine file size for URI " + uri);
    }

    public static void postVideo(Context context, String eventId, Uri videoUri, File cacheDir) throws Exception {

        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(context, videoUri);

        int videoWidth = Integer.parseInt(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH));
        int videoHeight = Integer.parseInt(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT));

        Bitmap frame = retriever.getFrameAtTime();

        Bitmap thumbnailBitmap = UI.scaledDownBitmap(frame, MAX_THUMBNAIL_EDGE_PIXELS);
        File thumbnail = UI.writeJPEGFromBitmap(thumbnailBitmap, THUMBNAIL_JPEG_QUALITY, cacheDir);

        Bitmap decentBitmap = UI.scaledDownBitmap(frame, MAX_DECENT_EDGE_PIXELS);
        File decent = UI.writeJPEGFromBitmap(decentBitmap, DECENT_JPEG_QUALITY, cacheDir);

        String assetId = postAssetData(context, videoUri, ContentType.MPEG4);
        String thumbnailId = postAssetData(context, Uri.fromFile(thumbnail), ContentType.JPEG);
        String decentId = postAssetData(context, Uri.fromFile(decent), ContentType.JPEG);
        String captureId = postCapture(eventId);

        long fileSize = getFileSize(context, videoUri);

        postAssetMetadataToCapture(eventId, assetId, captureId, fileSize, videoWidth, videoHeight, ContentType.MPEG4);
        postPhotoAssetMetadataToCapture(eventId, thumbnail, thumbnailId, captureId);
        postPhotoAssetMetadataToCapture(eventId, decent, decentId, captureId);
    }

    static class ContentType {
        static final String MPEG4 = "video/mp4";
        static String JPEG = "image/jpeg";

        static boolean isVideo(String contentType) {
            return contentType.startsWith("video/");
        }
    }

    static final int MAX_THUMBNAIL_EDGE_PIXELS = 200;
    static final int THUMBNAIL_JPEG_QUALITY = 70;

    static final int MAX_DECENT_EDGE_PIXELS = 1000;
    static final int DECENT_JPEG_QUALITY = 70;

    public static void downloadToFile(String url, File file) throws Exception {
        L.debug("GET TO FILE: " + url + " to file " + file);
        HttpURLConnection conn = openConnection(url);
        try {
            //d("Response Code: " + conn.getResponseCode());
            InputStream in = conn.getInputStream();
            try {
                FileOutputStream out = new FileOutputStream(file);
                try {
                    IOUtils.copy(in, out);
                    L.debug("GET TO FILE RETURNED: " + url);
                } finally {
                    out.close();
                }
            } finally {
                in.close();
            }
        } finally {
            conn.disconnect();
        }
    }
}