package com.funsocialapps.stitch;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;


public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
    }

    public void sendFeedback(View v) {

        String app = getString(R.string.app_name);
        String version = getString(R.string.gradleGeneratedVersionName);
        String model = Build.MODEL;
        String os = Build.VERSION.RELEASE;

        String emailAddress = "stitch@funsocialapps.com";
        String subject = getString(R.string.feedback_email_subject);
        String body = String.format(getString(R.string.feedback_email_body_app_version_model_os), app, version, model, os);

        String mailtoString = String.format("mailto:%s?subject=%s&body=%s", emailAddress, Uri.encode(subject), Uri.encode(body));
        Uri mailtoUri = Uri.parse(mailtoString);

        Intent intent = new Intent(Intent.ACTION_SENDTO, mailtoUri);

        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            L.error("Activity not found while trying to send feecback email.", e);
            Toast.makeText(this, R.string.no_email_apps, Toast.LENGTH_SHORT).show();
        }
    }
}
