package com.funsocialapps.stitch;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.http.HttpResponseCache;

import java.io.File;
import java.io.IOException;

/**
 * Created by doug on 5/20/15.
 */
public class App extends Application {

    private static Application sharedInstance;

    public App() {
        sharedInstance = this;
    }

    public static Application getSharedInstance() {
        return sharedInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        initDeviceId();

        try {
            File httpCacheDir = new File(getApplicationContext().getCacheDir(), "http");
            L.debug("Using HTTP cache at " + httpCacheDir);
            long httpCacheSize = 200 * 1000 * 1000; // 200 MB
            HttpResponseCache.install(httpCacheDir, httpCacheSize);
        } catch (IOException e) {
            L.error("HTTP response cache installation failed.", e);
        }
    }

    private void initDeviceId() {
        String deviceIDFilename = "device_id";
        SharedPreferences settings = getSharedPreferences(deviceIDFilename, Context.MODE_PRIVATE);
        String key = "deviceID";
        String id = null;
        try {
            id = settings.getString(key, null);
        } catch (Exception e) {
            L.error("Exception reading " + key + " from shared preferences. Will create a new ID", e);
        }
        if (id == null) {
            id = WebAPI.getUniqueId();
            settings.edit().putString(key, id).apply();
        }
        WebAPI.initDeviceId(id);
    }
}
