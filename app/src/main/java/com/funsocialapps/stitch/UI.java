package com.funsocialapps.stitch;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.MetadataException;
import com.drew.metadata.exif.ExifIFD0Directory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by doug on 5/21/15.
 */
public class UI {
    public static int dipToPx(Context context, int dip) {
        // From http://developer.android.com/guide/practices/screens_support.html#dips-pels
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int)(scale * dip + 0.5f);
    }

    public static Matrix rotationMatrixFromEXIFOrientation(int orientation) {

        //L.debug("orientation for matrix is " + orientation);

        Matrix matrix = new Matrix();

        switch (orientation) {
            case ExifInterface.ORIENTATION_NORMAL:
            case ExifInterface.ORIENTATION_UNDEFINED:
                break;
            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                matrix.setScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                matrix.setRotate(180);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_TRANSPOSE:
                matrix.setRotate(90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_TRANSVERSE:
                matrix.setRotate(-90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.setRotate(-90);
                break;
            default:
                L.error("Unknown EXIF orientation " + orientation);
                break;
        }

        return matrix;
    }

    public static Bitmap rotateBitmap(Bitmap bitmap, int orientation) {
        Matrix matrix = rotationMatrixFromEXIFOrientation(orientation);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    // Environment.DIRECTORY_PICTURES
    private static File createExternalFileForCapture(String suffix, String publicDirectory) throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        String imageFileName = timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(publicDirectory);
        storageDir.mkdirs();
        return File.createTempFile(imageFileName, suffix, storageDir);
    }

    public static File createExternalFileForImageCapture() throws IOException {
        // Create an image file name
       return createExternalFileForCapture(".jpg", Environment.DIRECTORY_PICTURES);
    }

    public static File createExternalFileForVideoCapture() throws IOException {
        // Create an image file name
        return createExternalFileForCapture(".mp4", Environment.DIRECTORY_MOVIES);
    }

    public static void galleryAddMedia(Context context, Uri media) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        mediaScanIntent.setData(media);
        context.sendBroadcast(mediaScanIntent);
    }

    public static void galleryAddMedia(Context context, File media) {
        Uri contentUri = Uri.fromFile(media);
        galleryAddMedia(context, contentUri);
    }

    public static Bitmap rotatedScaledBitmapFromPictureFile(Context context, Uri pictureUri, int maxPixels) throws IOException {
        int orientation = exifOrientationFromUri(context, pictureUri);

        InputStream s1 = context.getContentResolver().openInputStream(pictureUri);
        try {
            InputStream s2 = context.getContentResolver().openInputStream(pictureUri);
            try {
                final Bitmap bitmapUnrotated = UI.decodeBitmapStream(s1, s2, WebAPI.MAX_THUMBNAIL_EDGE_PIXELS * WebAPI.MAX_THUMBNAIL_EDGE_PIXELS);
                if (bitmapUnrotated == null) {
                    L.error("Couldn't decode picture " + pictureUri);
                }

                final Bitmap bitmap = UI.rotateBitmap(bitmapUnrotated, orientation);
                if (bitmap == null) {
                    L.error("Couldn't rotate picture");
                }

                return bitmap;
            } finally {
                s2.close();
            }
        } finally {
            s1.close();
        }
    }

    private static Bitmap decodeBitmapUri(Context context, Uri uri, int maxPixels) throws IOException {
        InputStream s1 = context.getContentResolver().openInputStream(uri);
        try {
            InputStream s2 = context.getContentResolver().openInputStream(uri);
            try {
                return decodeBitmapStream(s1, s2, maxPixels);
            } finally {
                s2.close();
            }
        } finally {
            s1.close();
        }
    }

    // Like BitmapFactory.decodeStream, but allows you to specify the max number of pixels
    // that the decoded bitmap should occupy. This allows you to read arbitary bitmaps without
    // worrying about the OOM errors because of a single high resolution bitmap.
    // Takes two InputStreams to the same file so that we don't have to rely on mark/reset, which requires
    // an arbitrary length readlimit to mark (and also isn't available on all InputStreams, which means you'd
    // have to wrap it in a BufferedInputStream).
    private static Bitmap decodeBitmapStream(InputStream inputStreamForFinalDecode, InputStream inputStreamForInitialDecode, int maxPixels) throws IOException {

        final BitmapFactory.Options options = new BitmapFactory.Options();

        options.inJustDecodeBounds = true;

        BitmapFactory.decodeStream(inputStreamForInitialDecode, null, options);

        if (options.outHeight == -1 || options.outWidth == -1) {
            L.error("Error decoding bitmap stream");
            return null;
        }

        double width = options.outWidth;
        double height = options.outHeight;

        int sampleSize = 1;
        for (double pixels = width * height; pixels > maxPixels; ) {
            sampleSize *= 2;
            double sampleSizeSquared = sampleSize*sampleSize;
            pixels = Math.ceil(width * height / sampleSizeSquared);
        }

        //L.debug("Decoding bitmap with sampleSize = " + sampleSize);
        options.inJustDecodeBounds = false;
        options.inSampleSize = sampleSize;

        return BitmapFactory.decodeStream(inputStreamForFinalDecode, null, options);
    }

    public static Bitmap scaledDownBitmap(Bitmap b, int maxEdge) {
        double scale = Math.min(((double) maxEdge) / Math.max(b.getWidth(), b.getHeight()), 1.0);
        Matrix m = new Matrix();
        m.postScale((float) scale, (float) scale);
        return Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), m, true);
    }

    private static int exifOrientationFromUri(Context context, Uri uri) throws IOException {
        try {
            InputStream in = context.getContentResolver().openInputStream(uri);
            try {
                Metadata metadata = ImageMetadataReader.readMetadata(in);
                Directory directory = metadata.getFirstDirectoryOfType(ExifIFD0Directory.class);
                if (directory != null && directory.containsTag(ExifIFD0Directory.TAG_ORIENTATION)) {
                    return directory.getInt(ExifIFD0Directory.TAG_ORIENTATION);
                }
            } finally {
                in.close();
            }
        } catch (MetadataException e) {
            // Ignore metadata exception.
            L.error("Ignoring metadata exception while looking for EXIF orientation.", e);
        } catch (ImageProcessingException e) {
            L.error("Ignoring image processing exception while looking for EXIF orientation.", e);
        }

        return ExifInterface.ORIENTATION_NORMAL;
    }

    public static Bitmap scaledDownRotatedBitmapFromJPEG(Context context, Uri jpegUri, int maxEdge) throws IOException {

        int orientation = exifOrientationFromUri(context, jpegUri);
        Matrix m = rotationMatrixFromEXIFOrientation(orientation);

        int maxTotalPixels = maxEdge * maxEdge;
        Bitmap b = decodeBitmapUri(context, jpegUri, maxTotalPixels);
        double scale = Math.min(((double) maxEdge) / Math.max(b.getWidth(), b.getHeight()), 1.0);
        m.postScale((float) scale, (float) scale);
        return Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), m, true);
    }

    public static File writeJPEGFromBitmap(Bitmap bitmap, int jpegQuality, File outputDir) throws Exception {
        File resultFile = File.createTempFile("scaled-rotated", "jpg", outputDir);
        try {
            FileOutputStream resultOut = new FileOutputStream(resultFile);
            try {
                boolean ok = bitmap.compress(Bitmap.CompressFormat.JPEG, jpegQuality, resultOut);
                if (!ok) {
                    throw new Exception("Error compressing scaled bitmap into JPEG.");
                }
            } finally {
                resultOut.close();
            }
        } catch(Exception e) {
            resultFile.delete();
            throw e;
        }
        return resultFile;
    }

    public static File scaledDownRotatedJPEGFile(Context context, Uri input, File outputDir, int maxEdgePixels, int jpegQuality) throws Exception {
        return writeJPEGFromBitmap(scaledDownRotatedBitmapFromJPEG(context, input, maxEdgePixels), jpegQuality, outputDir);
    }
}
