package com.funsocialapps.stitch;

import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by doug on 5/20/15.
 */
public class IOUtils {
    public static void copy(InputStream in, OutputStream out) throws java.io.IOException {
        byte[] buf = new byte[8192];
        int count;
        while((count = in.read(buf)) != -1) {
            //L.debug("Read " + count);
            out.write(buf, 0, count);
        }
    }

    public static String readUTF8Stream(InputStream in) throws java.io.IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream(8192);
        copy(in, out);
        String result = out.toString("UTF-8");
        //L.debug("got json: " + result);
        return result;
    }
}
