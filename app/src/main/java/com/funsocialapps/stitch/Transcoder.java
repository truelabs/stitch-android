package com.funsocialapps.stitch;

import android.content.Context;
import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaCodecList;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.media.MediaMetadataRetriever;
import android.media.MediaMuxer;
import android.net.Uri;
import android.os.Build;

import java.io.File;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by doug on 6/11/15.
 */
public class Transcoder {

    static final private int AUDIO_BIT_RATE = 128000;
    static final private int VIDEO_BIT_RATE = 1000000;
    static final private int VIDEO_FRAME_RATE = 30;
    static final private int VIDEO_I_FRAME_INTERVAL_IN_SECONDS = 10;

    static final int TIMEOUT_USEC = 1000;

    // TODO: Test with audio only file
    // TODO: Test with video only file

    public static void transcodeVideo(Context context, Uri originalVideoUri, File outputFile) throws Exception {

        //
        // Use MediaMetadataRetriever to get video orientation.
        //
        MediaMetadataRetriever metadataRetriever = new MediaMetadataRetriever();
        metadataRetriever.setDataSource(context, originalVideoUri);
        String rotationString = metadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION);
        int originalVideoRotation = -1;
        if (rotationString != null) {
            originalVideoRotation = Integer.parseInt(rotationString);
        }
        L.debug("Original Video Rotation is: " + originalVideoRotation);

        //
        // Setup MediaExtractor to demux source container
        //
        MediaExtractor extractor = new MediaExtractor();
        extractor.setDataSource(context, originalVideoUri, null);
        int originalTrackCount = extractor.getTrackCount();

        MediaFormat originalVideoFormat = null;
        int originalVideoTrack = -1;
        MediaCodec videoDecoder = null;

        MediaFormat originalAudioFormat = null;
        int originalAudioTrack = -1;
        MediaCodec audioDecoder = null;

        for(int i = 0; i < originalTrackCount; ++i) {
            MediaFormat format = extractor.getTrackFormat(i);
            String mime = format.getString(MediaFormat.KEY_MIME);
            if (mime.startsWith("video/")) {
                originalVideoTrack = i;
                originalVideoFormat = format;
                videoDecoder = MediaCodec.createDecoderByType(mime);
                videoDecoder.configure(format, null, null, 0);
                videoDecoder.start();
            } else if (mime.startsWith("audio/")) {
                originalAudioTrack = i;
                originalAudioFormat = format;
                audioDecoder = MediaCodec.createDecoderByType(mime);
                audioDecoder.configure(format, null, null, 0);
                audioDecoder.start();
            }
        }

        if (originalAudioTrack != -1) {
            extractor.selectTrack(originalAudioTrack);
        }

        if (originalVideoTrack != -1 ) {
            extractor.selectTrack(originalVideoTrack);
        }

        MediaCodec videoEncoder = null;

        int outputVideoTrack = -1;
        if (originalVideoFormat != null ) {
            int width = originalVideoFormat.getInteger(MediaFormat.KEY_WIDTH);
            int height = originalVideoFormat.getInteger(MediaFormat.KEY_HEIGHT);
            String videoMimeType = MediaFormat.MIMETYPE_VIDEO_AVC;
            L.debug("MIME is " + videoMimeType);

            MediaCodecInfo codecInfo = selectEncoder(videoMimeType);
            int colorFormat = selectColorFormat(codecInfo, videoMimeType);

            MediaFormat videoFormat = MediaFormat.createVideoFormat(videoMimeType, width, height);
            videoFormat.setInteger(MediaFormat.KEY_COLOR_FORMAT, colorFormat);
            videoFormat.setInteger(MediaFormat.KEY_BIT_RATE, VIDEO_BIT_RATE);
            videoFormat.setInteger(MediaFormat.KEY_FRAME_RATE, VIDEO_FRAME_RATE);
            videoFormat.setInteger(MediaFormat.KEY_I_FRAME_INTERVAL, VIDEO_I_FRAME_INTERVAL_IN_SECONDS);

            L.debug("Video codec: " + codecInfo.getName() + "format: " + videoFormat);

            videoEncoder = MediaCodec.createByCodecName(codecInfo.getName());
            videoEncoder.configure(videoFormat, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
            videoEncoder.start();
        }

        MediaCodec audioEncoder = null;
        int outputAudioTrack = -1;
        if (originalAudioFormat != null) {
            String audioMimeType = MediaFormat.MIMETYPE_AUDIO_AAC;

            int sampleRate = originalAudioFormat.getInteger(MediaFormat.KEY_SAMPLE_RATE);
            L.debug("Original audio sample rate is " + sampleRate);

            MediaCodecInfo codecInfo = selectEncoder(audioMimeType);
            MediaFormat audioFormat = MediaFormat.createAudioFormat(audioMimeType, sampleRate, 1);
            audioFormat.setInteger(MediaFormat.KEY_BIT_RATE, AUDIO_BIT_RATE);

            L.debug("Audio codec: " + codecInfo.getName() + ", format: " + audioFormat);

            audioEncoder = MediaCodec.createByCodecName(codecInfo.getName());
            audioEncoder.configure(audioFormat, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
            audioEncoder.start();
        }

        List<QueuedEncodedData> preMuxerStartedEncodedDataQueue = new ArrayList<>();
        int videoOutputTrack = -1;
        int audioOutputTrack = -1;

        TranscodePair videoTranscode = new TranscodePair(videoEncoder, videoDecoder);
        TranscodePair audioTranscode = new TranscodePair(audioEncoder, audioDecoder);

        //
        // Use MediaMuxer to mux streams into MPEG-4 container.
        //
        MediaMuxer muxer = null;

        boolean hasMoreSamples = true;

        int loopCounter = 0;

        while(hasMoreSamples || videoTranscode.keepRunning || videoTranscode.hasMoreDataToWrite || audioTranscode.keepRunning || audioTranscode.hasMoreDataToWrite) {
            if (loopCounter++ % 1000 == 0) {
                L.debug("In transcode loop " + loopCounter + " moreSamples=" + hasMoreSamples +
                        ", moreVideoToTrans=" + videoTranscode.keepRunning + ", moreVideoToWrite=" + videoTranscode.hasMoreDataToWrite +
                        ", moreAudioToTrans=" + audioTranscode.keepRunning + ", moreAudioToWrite=" + audioTranscode.hasMoreDataToWrite);
            }

            //
            // Copy samples from the input container to the video and audio decoders.
            //
            int trackIndex = extractor.getSampleTrackIndex();
            if (trackIndex >= 0) {
                if (trackIndex == originalVideoTrack) {
                    videoTranscode.addCurrentSampleToDecoder(extractor);
                } else if (trackIndex == originalAudioTrack) {
                    audioTranscode.addCurrentSampleToDecoder(extractor);
                } else {
                    // Shouldn't get here unless you select a track and forget to handle it.
                    L.error("Unexpected track index " + trackIndex);
                    extractor.advance();
                }
            } else {

                if (hasMoreSamples) {

                    // No more tracks available.
                    L.debug("hasMoreSamples = false");
                    hasMoreSamples = false;

                    videoTranscode.sendEndOfStream(extractor.getSampleTime());
                    audioTranscode.sendEndOfStream(extractor.getSampleTime());
                }
            }

            //
            // Copy video decoder output to video encoder input.
            //

            if (videoTranscode.keepRunning) {
                videoTranscode.copyDecoderOutputToEncoderInput();
            }

            if (audioTranscode.keepRunning) {
                audioTranscode.copyDecoderOutputToEncoderInput();
            }

            if (videoTranscode.hasMoreDataToWrite) {
                videoTranscode.copyEncoderOutputToMuxerOrQueue(videoOutputTrack, muxer, preMuxerStartedEncodedDataQueue);
            }

            if (videoTranscode.hasMoreDataToWrite) {
                audioTranscode.copyEncoderOutputToMuxerOrQueue(audioOutputTrack, muxer, preMuxerStartedEncodedDataQueue);
            }

            if (muxer == null) {
                if (videoTranscode.outputFormat != null && audioTranscode.outputFormat != null) {
                    L.debug("Starting muxer");
                    muxer = new MediaMuxer(outputFile.toString(), MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4);

                    if (originalVideoRotation != -1) {
                        muxer.setOrientationHint(originalVideoRotation);
                    }

                    videoOutputTrack = muxer.addTrack(videoTranscode.outputFormat);
                    audioOutputTrack = muxer.addTrack(audioTranscode.outputFormat);
                    muxer.start();

                    L.debug("Processing " + preMuxerStartedEncodedDataQueue.size() + " encoded data items.");

                    // Add enqueued data.
                    for(QueuedEncodedData data : preMuxerStartedEncodedDataQueue) {
                        int track;
                        if (data.transcodePair == videoTranscode) {
                            track = videoOutputTrack;
                        } else if (data.transcodePair == audioTranscode) {
                            track = audioOutputTrack;
                        } else {
                            throw new RuntimeException("Unexpected transcode pair");
                        }

                        // TODO: Need to set data buffer like I do above?
                        muxer.writeSampleData(track, data.buffer, data.bufferInfo);
                    }

                    preMuxerStartedEncodedDataQueue.clear();
                    preMuxerStartedEncodedDataQueue = null;
                }
            }
        }

        L.debug("Finished transcoding loop. Shutting down.");

        extractor.release();

        if (videoEncoder != null) {
            videoEncoder.stop();
            videoEncoder.release();
        }

        if (audioEncoder != null) {
            audioEncoder.stop();
            audioEncoder.release();
        }

        if (videoDecoder != null) {
            videoDecoder.stop();
            videoDecoder.release();
        }

        if (audioDecoder != null) {
            audioDecoder.stop();
            audioDecoder.release();
        }

        muxer.stop();
        muxer.release();

        L.debug("Stopped muxer");
    }

    static void dumpVideoEncoderInfo(boolean onlyEncoders) {

        int count = MediaCodecList.getCodecCount();
        L.debug("Dumping " + count + " items from MediaCodecList:");

        StringBuilder builder = new StringBuilder();

        for(int i = 0; i < count; ++i) {
            MediaCodecInfo info = MediaCodecList.getCodecInfoAt(i);

            if (onlyEncoders && !info.isEncoder()) {
                continue;
            }

            builder.setLength(0);

            builder.append(info.isEncoder() ? "Encoder" : "Decoder");
            builder.append("#" + i + ": " + info.getName() + " types: ");
            for(String type : info.getSupportedTypes()) {
                builder.append("{");
                builder.append(type + " ");
                MediaCodecInfo.CodecCapabilities caps = info.getCapabilitiesForType(type);
                if (caps != null) {
                    builder.append("colors=(");
                    for(int colorFormat : caps.colorFormats) {
                        builder.append(colorFormat);
                        builder.append(",");
                    }
                    builder.append(") ");

                    builder.append("profileLevels=(");
                    for(MediaCodecInfo.CodecProfileLevel profileLevel : caps.profileLevels) {
                        builder.append(profileLevel.profile + "_" + profileLevel.level + ", ");
                    }
                    builder.append(")");
                    if (Build.VERSION.SDK_INT >= 21) {
                        MediaCodecInfo.VideoCapabilities videoCaps = caps.getVideoCapabilities();
                        if (videoCaps != null) {
                            builder.append(" bitrates=" + videoCaps.getBitrateRange());
                            builder.append(" framerates=" + videoCaps.getSupportedFrameRates());
                            builder.append(" heights=" + videoCaps.getSupportedHeights());
                            builder.append(" widths=" + videoCaps.getSupportedWidths());
                        }
                    }

                }
                builder.append("} ");
            }

            L.debug(builder.toString());
        }
    }

    private static MediaCodecInfo selectEncoder(String mimeType) throws Exception {
        ArrayList<MediaCodecInfo> matching = new ArrayList<>();
        int numCodecs = MediaCodecList.getCodecCount();
        for (int i = 0; i < numCodecs; i++) {
            MediaCodecInfo codecInfo = MediaCodecList.getCodecInfoAt(i);
            if (!codecInfo.isEncoder()) {
                continue;
            }
            String[] types = codecInfo.getSupportedTypes();
            for (int j = 0; j < types.length; j++) {
                if (types[j].equalsIgnoreCase(mimeType)) {
                    return codecInfo;
                }
            }
        }
        throw new Exception("No codec found for mimeType " + mimeType);
    }

    /**
     * Returns a color format that is supported by the codec and by this test code.  If no
     * match is found, this throws a test failure -- the set of formats known to the test
     * should be expanded for new platforms.
     */
    private static int selectColorFormat(MediaCodecInfo codecInfo, String mimeType) throws Exception {
        MediaCodecInfo.CodecCapabilities capabilities = codecInfo.getCapabilitiesForType(mimeType);
        for (int i = 0; i < capabilities.colorFormats.length; i++) {
            int colorFormat = capabilities.colorFormats[i];
            if (isRecognizedFormat(colorFormat)) {
                return colorFormat;
            }
        }
        throw new Exception("Couldn't find a good color format for " + codecInfo.getName() + " / " + mimeType);
    }
    /**
     * Returns true if this is a color format that this test code understands (i.e. we know how
     * to read and generate frames in this format).
     */
    private static boolean isRecognizedFormat(int colorFormat) {
        switch (colorFormat) {
            // these are the formats we know how to handle for this test
            case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420Planar:
            case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420PackedPlanar:
            case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420SemiPlanar:
            case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420PackedSemiPlanar:
            case MediaCodecInfo.CodecCapabilities.COLOR_TI_FormatYUV420PackedSemiPlanar:
                return true;
            default:
                return false;
        }
    }

    static class TranscodePair {
        MediaCodec.BufferInfo decoderOutputBufferInfo = new MediaCodec.BufferInfo();
        ByteBuffer decoderOutputBuffer;
        int decoderOutputBufferIndex = -1;

        ByteBuffer encoderInputBuffer;
        int encoderInputBufferIndex = -1;

        MediaCodec.BufferInfo bufferInfo = new MediaCodec.BufferInfo();;

        MediaFormat outputFormat;

        MediaCodec encoder;
        MediaCodec decoder;

        boolean keepRunning;
        boolean hasMoreDataToWrite;

        TranscodePair(MediaCodec encoder, MediaCodec decoder) {
            this.encoder = encoder;
            this.decoder = decoder;
            hasMoreDataToWrite = keepRunning = encoder != null && decoder != null;
        }

        void addCurrentSampleToDecoder(MediaExtractor extractor) {

            if (decoder == null || encoder == null) return;

            int i = decoder.dequeueInputBuffer(TIMEOUT_USEC);
            if (i >= 0) {
                ByteBuffer sample = decoder.getInputBuffers()[i];
                int sampleSize = extractor.readSampleData(sample, 0);
                decoder.queueInputBuffer(i, 0, sampleSize, extractor.getSampleTime(), extractor.getSampleFlags());
                extractor.advance();
                // L.debug("Extracted sample of size " + sampleSize);
            } else {
                // no input buffer available, try again next time around the loop.
            }
        }

        void copyDecoderOutputToEncoderInput() {
            if (decoder == null || encoder == null) return;

            // Try to get an available decoder output buffer if you don't already have one.
            if (decoderOutputBuffer == null) {
                decoderOutputBufferIndex = decoder.dequeueOutputBuffer(decoderOutputBufferInfo, TIMEOUT_USEC);
                if (decoderOutputBufferIndex >= 0) {
                    decoderOutputBuffer = decoder.getOutputBuffers()[decoderOutputBufferIndex];
                }
            }

            // Try to get an available encoder input buffer if you don't already have one.
            if (encoder != null && encoderInputBuffer == null) {
                encoderInputBufferIndex = encoder.dequeueInputBuffer(TIMEOUT_USEC);
                if (encoderInputBufferIndex >= 0) {
                    encoderInputBuffer = encoder.getInputBuffers()[encoderInputBufferIndex];
                }
            }

            // If you have a decoder output buffer and a encoder input buffer, send the decoder output
            // to the encoder input.
            if (encoderInputBuffer != null && decoderOutputBuffer != null) {

                if ((decoderOutputBufferInfo.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
                    L.debug("keepRunning = false");
                    keepRunning = false;
                }

                // L.debug("Copying decoded video to encoder");

                encoderInputBuffer.put(decoderOutputBuffer);
                decoder.releaseOutputBuffer(decoderOutputBufferIndex, false);
                decoderOutputBuffer = null;
                decoderOutputBufferIndex = -1;
                encoder.queueInputBuffer(encoderInputBufferIndex, decoderOutputBufferInfo.offset, decoderOutputBufferInfo.size, decoderOutputBufferInfo.presentationTimeUs, decoderOutputBufferInfo.flags);
                encoderInputBuffer = null;
                encoderInputBufferIndex = -1;
            }
        }

        void copyEncoderOutputToMuxerOrQueue(int outputTrack, MediaMuxer muxer, List<QueuedEncodedData> preMuxerStartEncodedDataQueue) {
            if (decoder == null || encoder == null) return;

            int i = encoder.dequeueOutputBuffer(bufferInfo, TIMEOUT_USEC);
            if (i == MediaCodec.INFO_TRY_AGAIN_LATER) {
                // Try again
                //L.debug("Try again later video encoder dequeue output buffer");
            } else if(i == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
                L.debug("Output format changed. Adding track to muxer.");
                outputFormat = encoder.getOutputFormat();
            } else if (i == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
                // Buffers changed. Oh well, we ask for them every time.
                L.debug("Output buffers changed");
            } else if (i < 0) {
                L.error("Unexpected status from encoder: " + i);
                // ignoring
            } else {

                ByteBuffer encodedData = encoder.getOutputBuffers()[i];

                if ((bufferInfo.flags & MediaCodec.BUFFER_FLAG_CODEC_CONFIG) != 0) {
                    // Codec config was pulled out and fed to the muxer during INFO_OUTPUT_FORMAT_CHANGED. Ignore it.
                    L.debug("Ignoring BUFFER_FLAG_CODEC_CONFIG");
                    bufferInfo.size = 0;
                }

                if (bufferInfo.size != 0) {
                    // L.debug("Writing " + videoEncoderOutputBufferInfo.size + " video output bytes to muxer");

                    // needed?
                    encodedData.position(bufferInfo.offset);
                    encodedData.limit(bufferInfo.offset + bufferInfo.size);

                    if (muxer != null) {
                        muxer.writeSampleData(outputTrack, encodedData, bufferInfo);
                    } else {
                        ByteBuffer buffer = ByteBuffer.allocate(encodedData.capacity());
                        buffer.put(encodedData);
                        MediaCodec.BufferInfo bi = new MediaCodec.BufferInfo();
                        bi.set(bufferInfo.offset, bufferInfo.size, bufferInfo.presentationTimeUs, bufferInfo.flags);
                        QueuedEncodedData data = new QueuedEncodedData(buffer, bufferInfo, this);
                        preMuxerStartEncodedDataQueue.add(data);
                    }
                }

                encoder.releaseOutputBuffer(i, false);

                if ((bufferInfo.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
                    L.debug("hasMoreVideoToWrite = false");
                    hasMoreDataToWrite = false;
                }
            }
        }

        void sendEndOfStream(long sampleTime) {
            if (decoder == null || encoder == null) return;

            while (true) {
                int i = decoder.dequeueInputBuffer(TIMEOUT_USEC);
                if (i >= 0) {
                    decoder.queueInputBuffer(i, 0, 0, sampleTime, MediaCodec.BUFFER_FLAG_END_OF_STREAM);
                    L.debug("End of Stream sent for " + decoder);
                    return;
                }
            }
        }
    }

    static class QueuedEncodedData {
        ByteBuffer buffer;
        MediaCodec.BufferInfo bufferInfo;
        TranscodePair transcodePair;

        QueuedEncodedData(ByteBuffer buffer, MediaCodec.BufferInfo bufferInfo, TranscodePair transcodePair) {
            this.buffer = buffer;
            this.bufferInfo = bufferInfo;
            this.transcodePair = transcodePair;
        }
    }
}
