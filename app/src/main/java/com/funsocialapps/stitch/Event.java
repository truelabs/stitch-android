package com.funsocialapps.stitch;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by doug on 5/21/15.
 */

class Event implements Parcelable {
    final String name;
    final String id;

    Event(String name, String id) {
        this.name = name;
        this.id = id;
    }

    public String toString() { return name; }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(id);
    }

    public static final Parcelable.Creator<Event> CREATOR
            = new Parcelable.Creator<Event>() {
        public Event createFromParcel(Parcel in) {
            return new Event(in);
        }

        public Event[] newArray(int size) {
            return new Event[size];
        }
    };

    private Event(Parcel in) {
        name = in.readString();
        id = in.readString();
    }
}