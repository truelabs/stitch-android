package com.funsocialapps.stitch;

import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by doug on 5/26/15.
 */
public class RecentEvents implements DatabaseErrorHandler {

    private SQLiteDatabase db;

    RecentEvents(File dbfile) {
        reinit(dbfile);
    }

    private void reinit(File dbfile) {
        dbfile.getParentFile().mkdirs();
        db = SQLiteDatabase.openOrCreateDatabase(dbfile.getPath(), null, this);

        if (db.getVersion() == 0) {
            // First migration
            db.beginTransaction();
            try {
                db.execSQL("CREATE TABLE events (id TEXT PRIMARY KEY, name TEXT NOT NULL, accessed DATETIME NOT NULL)");
                db.execSQL("CREATE INDEX events_accessed ON events(accessed)");
                db.setVersion(1);
                db.setTransactionSuccessful();
            } finally {
                db.endTransaction();
            }
        }
    }

    public void accessed(Event event) {
        Object[] args = {event.id, event.name, (new Date()).getTime()};
        db.execSQL("INSERT OR IGNORE INTO events (id,name,accessed) VALUES (?,?,?)", args);
    }

    public List<Event> events() {
        String[] selectionArgs = {};
        Cursor cursor = db.rawQuery("SELECT id,name FROM events ORDER BY accessed DESC", selectionArgs);

        int idIndex = cursor.getColumnIndex("id");
        int nameIndex = cursor.getColumnIndex("name");

        ArrayList<Event> result = new ArrayList<Event>();

        while (cursor.moveToNext()) {
            String id = cursor.getString(idIndex);
            String name = cursor.getString(nameIndex);
            result.add(new Event(name, id));
        }

        return result;
    }

    @Override
    public void onCorruption(SQLiteDatabase dbObj) {
        L.error("Database corruption. Deleting database.");
        File file = new File(db.getPath());
        if (!file.delete()) {
            L.debug("Error deleting database file during rebuild.");
        }
        db = null;
        reinit(file);
    }
}
