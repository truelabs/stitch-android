package com.funsocialapps.stitch;

import android.util.Log;

/**
 * Created by doug on 5/27/15.
 */
public class L {
    public static final String tag = "Stitch";

    public static void debug(String msg) {
        Log.d(tag, msg);
    }

    public static void debug(String msg, Throwable throwable) {
        Log.d(tag, msg, throwable);
    }

    public static void error(String msg) {
        Log.e(tag, msg);
    }

    public static void error(String msg, Throwable throwable) {
        Log.e(tag, msg, throwable);
    }
}
