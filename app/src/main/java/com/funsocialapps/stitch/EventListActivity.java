package com.funsocialapps.stitch;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class EventListActivity extends AppCompatActivity implements
        SwipeRefreshLayout.OnRefreshListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    RecyclerView recyclerView;
    EventAdapter recyclerViewAdapter;
    JSONArray jsonArray;
    SwipeRefreshLayout swipeLayout;
    RecentEvents recentEventsDB;
    GoogleApiClient googleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_list);

        recyclerViewAdapter = new EventAdapter();
        recyclerView = (RecyclerView)findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(recyclerViewAdapter);

        setTitle(R.string.title_activity_event_list);

        swipeLayout = (SwipeRefreshLayout)findViewById(R.id.swipe_container);
        swipeLayout.setOnRefreshListener(this);

        File dbfile = new File(getFilesDir(), "events.db");
        L.debug("Using recents db file " + dbfile.toString());
        recentEventsDB = new RecentEvents(dbfile);

        L.debug("Building Google API Client");
        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();


        //Transcoder.dumpVideoEncoderInfo(true);
    }

    @Override
    protected void onStart() {
        super.onStart();
        googleApiClient.connect();
        reloadRecents();
    }

    @Override
    protected void onStop() {
        super.onStop();
        googleApiClient.disconnect();
    }

    private void reloadRecents() {
        Section section = recyclerViewAdapter.sectionForTag(RECENT_SECTION);
        if (section == null) {
            section = new Section(RECENT_SECTION, R.string.recent_events);
            recyclerViewAdapter.sections.add(section);
        }

        section.events = recentEventsDB.events();
        recyclerViewAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_event_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            refreshEvents();
            return true;
        }

        if (id == R.id.action_add) {
            addEvent();
            return true;
        }

        if (id == R.id.action_show_about) {
            showAbout();
            return true;
        }

        L.debug("Unhandled menu option in Events");

        return super.onOptionsItemSelected(item);
    }

    private void refreshEvents() {

        if (googleApiClient == null) {
            L.error("googleAPIClient null. Cannot refresh events.");
            return;
        }

        final Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (location == null) {
            L.error("Location is null. Cannot refresh events.");
            return;
        }

        final Handler mainHandler = new Handler();
        final Runnable stopRefreshing = new Runnable() {
            @Override
            public void run() {
                swipeLayout.setRefreshing(false);
            }
        };

        L.debug("refreshing events");
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String url = WebAPI.getEventsNearURL(location.getLatitude(), location.getLongitude());
                    final JSONArray json = WebAPI.getJSONArray(url);
                    mainHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            setJSON(json);
                        }
                    });
                } catch (Exception e) {
                    // Any number of things could go wrong. Just log and forget about it. Don't crash because
                    // of network errors.
                    L.error("Exception requesting events.", e);
                } finally {
                    mainHandler.post(stopRefreshing);
                }
            }
        }).start();
    }

    private void setJSON(JSONArray json) {
        if (Looper.getMainLooper().getThread() != Thread.currentThread()) {
            throw new java.lang.IllegalThreadStateException("Not on main thread");
        }

        try {
            Section section = recyclerViewAdapter.sectionForTag(NEARBY_SECTION);
            if (section == null) {
                section = new Section(NEARBY_SECTION, R.string.nearby_events);
                recyclerViewAdapter.sections.add(0, section);
            }

            section.events.clear();
            for (int i = 0; i < json.length(); ++i) {
                JSONObject o = json.getJSONObject(i);
                Event event = new Event(o.getString("name"), o.getString("id"));
                section.events.add(event);
            }

            recyclerViewAdapter.notifyDataSetChanged();
        }
        catch (Exception e) {
            L.error("Exception looking through events JSON.", e);
        }
    }

    @Override
    public void onRefresh() {
        L.debug("onRefresh called");
        refreshEvents();
    }

    private static final int ADD_EVENT_REQUEST = 1;

    private void addEvent() {

        if (googleApiClient == null) {
            // I don't think this should happen in real life.
            L.error("Can't add event because googleApiClient not set. Silently failing.");
            return;
        }

        final Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (location == null) {
            // Google says this will rarely be null, but can be.
            Toast.makeText(this, getString(R.string.cannot_determine_location), Toast.LENGTH_SHORT).show();
            return;
        }

        Intent intent = new Intent(this, AddEventActivity.class);
        intent.putExtra(AddEventActivity.LOCATION_EXTRA, location);
        startActivityForResult(intent, ADD_EVENT_REQUEST);
    }

    private void showEvent(Event event) {
        Intent intent = new Intent(this, EventActivity.class);
        intent.putExtra("event", event);
        startActivity(intent);
        recentEventsDB.accessed(event);
    }

    private void showAbout() {
        startActivity(new Intent(this, AboutActivity.class));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ADD_EVENT_REQUEST) {
            if (resultCode == RESULT_OK) {
                L.debug("Event was added. Showing.");
                Event event = data.getParcelableExtra("event");
                showEvent(event);
            } else {
                L.debug("add event returned something besides OK. " + resultCode);
            }
        }
    }

    final int NEARBY_SECTION = 0;
    final int RECENT_SECTION = 1;

    @Override
    public void onConnected(Bundle bundle) {
        L.debug("LOCATION: onConnected");
        refreshEvents();
    }

    @Override
    public void onConnectionSuspended(int i) {
        L.debug("LOCATION: onConnectionSuspended: " + i);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        L.debug("LOCATION: onConnectionFailed: " + connectionResult);
    }

    class Section {
        final int tag;
        final String headerText;
        List<Event> events;

        Section(int tag, int headerText) {
            this.tag = tag;
            this.headerText = getString(headerText);
            events = new ArrayList<Event>();
        }
    };

    class TextItemViewHolder extends RecyclerView.ViewHolder {
        TextView text;
        TextItemViewHolder(View viewItem) {
            super(viewItem);
            text = (TextView)itemView.findViewById(R.id.text_view);
        }
    }

    class EventAdapter extends RecyclerView.Adapter {

        ArrayList<Section> sections;

        final int HEADER = 0;
        final int EVENT = 1;

        EventAdapter() {
            sections = new ArrayList<Section>();
        }

        Section sectionForTag(int tag) {
            for(Section section : sections) {
                if (section.tag == tag) {
                    return section;
                }
            }
            return null;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            Object data = dataAtPosition(position);
            TextItemViewHolder textHolder = (TextItemViewHolder)holder;

            if (holder.getItemViewType() == HEADER) {
                String headerText = (String)data;
                textHolder.text.setText(headerText);
            } else if (holder.getItemViewType() == EVENT) {
                final Event event = (Event)data;
                textHolder.text.setText(event.name);
                textHolder.text.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showEvent(event);
                    }
                });
            }
        }

        @Override
        public int getItemCount() {
            int count = 0;
            for(Section section : sections) {
                count += 1 + section.events.size();
            }
            return count;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            if (viewType == HEADER || viewType == EVENT) {
                int layout = viewType == HEADER ? R.layout.list_header : R.layout.list_text_item;
                View view = (View)LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
                return new TextItemViewHolder(view);
            }

            assert false;
            return null;
        }

        @Override
        public int getItemViewType(int position) {
            Object data = dataAtPosition(position);
            if (data instanceof Event) {
                return EVENT;
            } else if (data instanceof String) {
                return HEADER;
            }
            assert false;
            return 0;
        }

        Object dataAtPosition(int position) {
            for(Section section : sections) {
                if(position == 0) {
                    return section.headerText;
                }
                --position;
                if(position >= 0 && position < section.events.size()) {
                    return section.events.get(position);
                }
                position -= section.events.size();
            }

            assert false;
            return null;
        }
    }
}
