package com.funsocialapps.stitch;

import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ShareActionProvider;
import android.view.Display;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.net.HttpURLConnection;
import java.util.ArrayList;

import static android.provider.MediaStore.ACTION_IMAGE_CAPTURE;
import static android.provider.MediaStore.ACTION_VIDEO_CAPTURE;
import static android.provider.MediaStore.EXTRA_OUTPUT;
import static android.provider.MediaStore.EXTRA_VIDEO_QUALITY;

public class EventActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    Event event;
    CaptureAdapter captures;
    SwipeRefreshLayout swipeLayout;
    UploadTaskBroadcastReceiver uploadTaskBroadcastReceiver;
    GridView gridView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);

        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        swipeLayout.setOnRefreshListener(this);

        captures = new CaptureAdapter(this);

        event = (Event) getIntent().getParcelableExtra("event");
        L.debug("Got passed via intent extra: " + event);

        if (event == null) {

            String name = "";
            String id = "";

            // Try to extract event ID from URL.
            Uri uri = getIntent().getData();
            if (uri != null) {
                String tmp = uri.getQueryParameter("event");
                if (tmp != null) {
                    id = tmp;
                }
            }

            event = new Event(name, id);
        }

        gridView = (GridView) findViewById(R.id.grid);
        gridView.setAdapter(captures);
        gridView.setMultiChoiceModeListener(multiChoiceModeCallback);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                gridItemClicked(position);
            }
        });

        setTitle(event.name);

        uploadTaskBroadcastReceiver = new UploadTaskBroadcastReceiver();

        refreshEvent();
    }

    @Override
    protected void onResume() {
        super.onResume();

        IntentFilter filter = new IntentFilter(MediaUploadIntentService.ACTION_BROADCAST);
        LocalBroadcastManager.getInstance(this).registerReceiver(uploadTaskBroadcastReceiver, filter);
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(uploadTaskBroadcastReceiver);
        super.onPause();
    }

    class UploadTaskBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            boolean status = intent.getBooleanExtra(MediaUploadIntentService.STATUS, false);
            L.debug("UploadTaskBroadcastReceiver received status " + status);
            if (status) {
                refreshEvent();
            } else {
                String originalAction = intent.getStringExtra(MediaUploadIntentService.ORIGINAL_ACTION);
                int resId = originalAction.equals(MediaUploadIntentService.ACTION_PHOTO) ? R.string.error_capturing_photo : R.string.error_capturing_video;
                Toast.makeText(EventActivity.this, resId, Toast.LENGTH_SHORT).show();
            }
        }
    }

    static final String STATE_CURRENT_MEDIA_FILE = "current_media_file";

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(STATE_CURRENT_MEDIA_FILE, currentMediaCaptureFile);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        currentMediaCaptureFile = (File)savedInstanceState.getSerializable(STATE_CURRENT_MEDIA_FILE);
    }

    @Override
    public void onRefresh() {
        L.debug("onRefresh called");
        refreshEvent();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_event, menu);

        // Locate MenuItem with ShareActionProvider
        MenuItem item = menu.findItem(R.id.action_share_event);

        // Fetch and store ShareActionProvider
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, event.name);
        shareIntent.putExtra(Intent.EXTRA_TEXT, WebAPI.shareEventURL(event.id));

        ShareActionProvider shareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(item);
        shareActionProvider.setShareIntent(shareIntent);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_add_photo) {
            capturePhoto();
            return true;
        }

        if (id == R.id.action_add_video) {
            captureVideo();
            return true;
        }

        if (id == R.id.action_select) {
            L.debug("action_select");

            //startSupportActionMode(selectModeCallback);
        }

        if (id == R.id.action_upload) {
            pickExistingMedia();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void refreshEvent() {
        final Handler handler = new Handler(Looper.getMainLooper());

        final Runnable stopRefreshing = new Runnable() {
            @Override
            public void run() {
                swipeLayout.setRefreshing(false);
            }
        };

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    final JSONObject obj = WebAPI.getJSONObject(WebAPI.getEventURL(event.id));
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            captures.clear();
                            try {
                                JSONArray captureArray = obj.getJSONArray("captures");
                                for (int i = 0; i < captureArray.length(); ++i) {
                                    try {
                                        JSONObject capture = captureArray.getJSONObject(i);
                                        captures.add(capture);
                                    } catch (JSONException e) {
                                        L.error("Exception adding capture to captures array adapter.", e);
                                    }
                                }
                            } catch (JSONException e) {
                                L.error("Exception processing captures", e);
                            }

                            // Get the event name. If this Activity was started from a URL, the name may not have been set initially.
                            try {
                                String eventName = obj.getJSONObject("event").getString("name");
                                event = new Event(eventName, event.id);
                                setTitle(event.name);
                            } catch (JSONException e) {
                                e.printStackTrace();
                                L.error("Exception getting event name", e);
                            }
                        }
                    });
                } catch (Exception e) {
                    L.error("Exception making request for event.", e);
                } finally {
                    handler.post(stopRefreshing);
                }
            }
        }).start();
    }

    int imageRequestCounter;

    private class CaptureAdapter extends ArrayAdapter<JSONObject> {
        CaptureAdapter(Context context) {
            super(context, 0 /* irrelavent since getView is overridden */);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            //L.debug("getView called for position " + position);
            View cell;
            if (convertView == null) {
                cell = getLayoutInflater().inflate(R.layout.grid_photo, parent, false);
            } else {
                cell = convertView;
            }

            final ImageView imageView = (ImageView)cell.findViewById(R.id.image);
            final ImageView videoIconView = (ImageView)cell.findViewById(R.id.videoIcon);
            final View itemCheckedOverlayView = cell.findViewById(R.id.itemCheckedOverlay);

            itemCheckedOverlayView.setVisibility(gridView.isItemChecked(position) ? View.VISIBLE : View.GONE);

            final int imageRequestId = ++imageRequestCounter;
            imageView.setTag(imageRequestId);

            imageView.setImageDrawable(new ColorDrawable(Color.GRAY));
            videoIconView.setVisibility(View.INVISIBLE);

            try {
                JSONObject capture = getItem(position);
                if (WebAPI.videoAssetForCapture(capture) != null) {
                    videoIconView.setVisibility(View.VISIBLE);
                }

                JSONObject asset = WebAPI.bestVariantForSize(capture, imageView.getWidth(), imageView.getHeight());
                if (asset == null) {
                    // Don't bother. You'll just generate a bunch of null pointer exceptions.
                    return cell;
                }
                final String assetId = asset.getString("id");
                final String url = WebAPI.getDataURL(assetId);
                final Handler handler = new Handler(getMainLooper());

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            HttpURLConnection conn = WebAPI.openConnection(url);
                            int responseCode = conn.getResponseCode();
                            if (responseCode == HttpURLConnection.HTTP_OK) {
                                File tmpFile = File.createTempFile("photo", null, getContext().getCacheDir());
                                try {
                                    FileOutputStream out = new FileOutputStream(tmpFile);
                                    try {
                                        IOUtils.copy(conn.getInputStream(), out);
                                    } finally {
                                        out.close();
                                    }

                                    final Bitmap bitmap = UI.rotatedScaledBitmapFromPictureFile(getApplicationContext(), Uri.fromFile(tmpFile), WebAPI.MAX_THUMBNAIL_EDGE_PIXELS*WebAPI.MAX_THUMBNAIL_EDGE_PIXELS);

                                    handler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            int req = (int)imageView.getTag();
                                            if (req == imageRequestId) {
                                                imageView.setImageBitmap(bitmap);
                                            } else {
                                                L.debug("Ignoring response because request IDs don't match. " + req + " != " + imageRequestId);
                                            }
                                        }
                                    });

                                } finally {
                                    tmpFile.delete();
                                }
                            } else {
                                L.debug("Got " + responseCode + " (expected 200) for " + url);
                            }
                        } catch (Exception e) {
                            L.error("Exception requesting " + url + ". ", e);
                        } finally {
                        }
                    }
                }).start();
            } catch (Exception e) {
                L.error("Exception reading JSON for capture. ", e);
            }

            return cell;
        }
    }

    private final static int REQUEST_CAPTURE_PHOTO = 1;
    private final static int REQUEST_CAPTURE_VIDEO = 2;
    private final static int REQUEST_CHOOSE_EXISTING_MEDIA = 3;
    private File currentMediaCaptureFile;


    private void capturePhoto() {

        currentMediaCaptureFile = null;

        try {
            Intent intent = new Intent(ACTION_IMAGE_CAPTURE);
            if (intent.resolveActivity(getPackageManager()) == null) {
                throw new Exception("Couldn't resolve image capture activity");
            }

            currentMediaCaptureFile = UI.createExternalFileForImageCapture();

            intent.putExtra(EXTRA_OUTPUT, Uri.fromFile(currentMediaCaptureFile));
            startActivityForResult(intent, REQUEST_CAPTURE_PHOTO);
        } catch (Exception e) {
            L.error("Exception trying to capture photo", e);
            Toast.makeText(this, R.string.error_capturing_photo, Toast.LENGTH_SHORT).show();
            if (currentMediaCaptureFile != null) {
                currentMediaCaptureFile.delete();
                currentMediaCaptureFile = null;
            }
        }
    }

    private void captureVideo() {

        currentMediaCaptureFile = null;

        try {
            Intent intent = new Intent(ACTION_VIDEO_CAPTURE);
            intent.putExtra(EXTRA_VIDEO_QUALITY, 1);
            if (intent.resolveActivity(getPackageManager()) == null) {
                throw new Exception("Couldn't resolve video capture activity");
            }

            currentMediaCaptureFile = UI.createExternalFileForVideoCapture();

            intent.putExtra(EXTRA_OUTPUT, Uri.fromFile(currentMediaCaptureFile));
            startActivityForResult(intent, REQUEST_CAPTURE_VIDEO);
        } catch (Exception e) {
            L.error("Exception trying to capture video", e);
            Toast.makeText(this, R.string.error_capturing_video, Toast.LENGTH_SHORT).show();
            if (currentMediaCaptureFile != null) {
                currentMediaCaptureFile.delete();
                currentMediaCaptureFile = null;
            }
        }
    }

    private void pickExistingMedia() {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("*/*");
        String[] types = {"image/jpeg", "video/*"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES, types);
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        startActivityForResult(intent, REQUEST_CHOOSE_EXISTING_MEDIA);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CAPTURE_PHOTO || requestCode == REQUEST_CAPTURE_VIDEO) {
            L.debug("Capture activity returned result code " + resultCode);
            if (resultCode == RESULT_OK) {
                L.debug("Successfully captured photo at " + currentMediaCaptureFile);
                UI.galleryAddMedia(this, currentMediaCaptureFile);

                if (requestCode == REQUEST_CAPTURE_PHOTO) {
                    MediaUploadIntentService.startPhotoUpload(this, event.id, Uri.fromFile(currentMediaCaptureFile));
                } else if (requestCode == REQUEST_CAPTURE_VIDEO) {
                    MediaUploadIntentService.startVideoUpload(this, event.id, Uri.fromFile(currentMediaCaptureFile));
                }
            } else {
                if (currentMediaCaptureFile != null) {
                    currentMediaCaptureFile.delete();
                }
            }
            currentMediaCaptureFile = null;
        } else if (requestCode == REQUEST_CHOOSE_EXISTING_MEDIA && resultCode == RESULT_OK) {
            L.debug("REQUEST_CHOOSE_EXISTING_MEDIA: getData=" + data.getData() + ", getClipData=" + data.getClipData());

            ClipData clipData = data.getClipData();
            if (clipData != null) {
                // Multiple items
                for(int i = 0; i < clipData.getItemCount(); ++i) {
                    ClipData.Item item = clipData.getItemAt(i);
                    Uri uri = item.getUri();
                    L.debug("Queuing upload for item " + i + ": " + uri + ", type: " + getContentResolver().getType(uri));
                    MediaUploadIntentService.startMediaUpload(this, event.id, uri);
                }
            } else {
                // Single item
                Uri uri = data.getData();
                L.debug("Queuing single item upload: " + uri + ", type=" + data.getType());
                MediaUploadIntentService.startMediaUpload(this, event.id, uri);
            }

        } else {
            L.error("Unhandled activity requestCode: " + requestCode);
        }
    }

    public void gridItemClicked(int position) {
        try {
            JSONObject capture = captures.getItem(position);
            JSONObject asset = WebAPI.videoAssetForCapture(capture);
            if (asset == null) {
                // no video, try photo.
                asset = fullscreenPhotoAssetForCapture(capture);
            }
            showVariant(asset);
        } catch(Exception e) {
            L.error("Error opening video or photo.", e);
            Toast.makeText(this, R.string.error_showing_item, Toast.LENGTH_SHORT).show();
        }
    }

    private JSONObject fullscreenPhotoAssetForCapture(JSONObject capture) throws JSONException {
        WindowManager wm = ((WindowManager) this.getSystemService(this.WINDOW_SERVICE));
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return WebAPI.bestVariantForSize(capture, size.x, size.y);
    }

    private void showVariant(JSONObject variant) throws JSONException {
        if (variant == null) {
            Toast.makeText(this, R.string.error_showing_item, Toast.LENGTH_SHORT).show();
            return;
        }

        String url = WebAPI.getDataURL(variant.getString("id"));
        Uri uri = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        intent.setDataAndType(uri, variant.getString("type"));
        startActivity(intent);
    }

    private ArrayList<JSONObject> checkedCaptures() {
        ArrayList<JSONObject> result = new ArrayList<>();
        for(int i = 0; i < gridView.getChildCount(); ++i) {
            if (gridView.isItemChecked(i)) {
                result.add(captures.getItem(i));
            }
        }
        return result;
    }

    private void shareCheckedCaptures() {
        ArrayList<Uri> mediaURIs = new ArrayList<Uri>();
        String typeForIntent = null;
        boolean multipleTypes = false;

        for (JSONObject capture : checkedCaptures()) {
            try {
                JSONObject variant = WebAPI.bestVariantForSize(capture, WebAPI.MAX_DECENT_EDGE_PIXELS, WebAPI.MAX_DECENT_EDGE_PIXELS);
                String assetURL = WebAPI.getDataURL(variant.getString("id"));
                String type = variant.getString("type");
                if (typeForIntent != null && !type.equals(typeForIntent)) {
                    multipleTypes = true;
                }
                typeForIntent = type;
                mediaURIs.add(Uri.parse(assetURL));
            } catch (Exception e) {
                L.error("Skipping capture. Got exception while looking for best variant for size.", e);
            }
        }

        if (multipleTypes) {
            typeForIntent = "*/*";
        }

        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND_MULTIPLE);
        shareIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, mediaURIs);
        shareIntent.setType(typeForIntent);
        startActivity(Intent.createChooser(shareIntent, getString(R.string.share_with)));
    }

    private void downloadCheckedCaptures() {
        final ArrayList<JSONObject> captures = checkedCaptures();

        // TODO: Move this to background intent.

        new Thread(new Runnable() {
            @Override
            public void run() {
                L.debug("Downloading captures...");

                File downloadDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);

                ArrayList<String> mimeTypes = new ArrayList<>();
                ArrayList<String> paths = new ArrayList<>();

                for(JSONObject capture : captures) {
                    try {
                        JSONObject asset = WebAPI.videoAssetForCapture(capture);
                        String suffix;
                        if (asset != null) {
                            suffix = ".mp4";
                        } else {
                            asset = WebAPI.bestVariantForSize(capture, WebAPI.MAX_DECENT_EDGE_PIXELS, WebAPI.MAX_DECENT_EDGE_PIXELS);
                            suffix = ".jpg";
                        }

                        String mimeType = asset.getString("type");
                        File downloadedFile = File.createTempFile("stitch", suffix, downloadDir);
                        String url = WebAPI.getDataURL(asset.getString("id"));
                        WebAPI.downloadToFile(url, downloadedFile);

                        //UI.galleryAddMedia(getApplicationContext(), downloadedFile);

                        mimeTypes.add(mimeType);
                        paths.add(downloadedFile.toString());

                    } catch (Exception e) {
                        L.error("Error processing capture: " + capture, e);
                    }
                }

                // Scan all files
                L.debug("Media Scanner scanning " + paths.size() + " files.");
                String[] pathsArray = paths.toArray(new String[paths.size()]);
                String[] mimeTypeArray = mimeTypes.toArray(new String[mimeTypes.size()]);
                MediaScannerConnection.scanFile(getApplicationContext(), pathsArray, mimeTypeArray, new MediaScannerConnection.OnScanCompletedListener() {
                    @Override
                    public void onScanCompleted(String path, Uri uri) {
                        L.debug("Scanned: " + path);
                    }
                });

            }
        }).start();
    }

    private AbsListView.MultiChoiceModeListener multiChoiceModeCallback = new AbsListView.MultiChoiceModeListener() {

        @Override
        public void onItemCheckedStateChanged(android.view.ActionMode mode, int position, long id, boolean checked) {
            L.debug("onItemCheckedStateChanged: checked=" + checked + ", position=" + position + ", id=" + id);
            captures.notifyDataSetChanged();
//            View overlay = gridView.getChildAt(position).findViewById(R.id.itemCheckedOverlay);
//            overlay.setVisibility(checked ? View.VISIBLE : View.GONE);
        }

        @Override
        public boolean onCreateActionMode(android.view.ActionMode mode, Menu menu) {
            L.debug("onCreateActionMode");
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.menu_select_items, menu);
            mode.setTitle(R.string.action_select);

            return true;
        }

        @Override
        public boolean onPrepareActionMode(android.view.ActionMode mode, Menu menu) {
            L.debug("onPrepareActionMode");
            return false;
        }

        @Override
        public boolean onActionItemClicked(android.view.ActionMode mode, MenuItem item) {
            L.debug("onActionItemClicked");

            int id = item.getItemId();

            if (id == R.id.action_share) {
                shareCheckedCaptures();
                mode.finish();
                return true;
            }

            if (id == R.id.action_download) {
                downloadCheckedCaptures();
                mode.finish();
                return true;
            }

            return false;
        }

        @Override
        public void onDestroyActionMode(android.view.ActionMode mode) {
            L.debug("onDestroyActionMode");
        }
    };
}