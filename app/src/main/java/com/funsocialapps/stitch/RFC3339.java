package com.funsocialapps.stitch;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by doug on 5/21/15.
 */
public class RFC3339 {

    public static DateFormat getDateFormat() {
        // SimpleDateFormat is not thread safe. It's format method mutates a calendar instance
        // variable. Therefore, create and return a new one each call. It's up to the caller
        // to cache they run into a performance issue.
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
        f.setTimeZone(TimeZone.getTimeZone("UTC"));
        return f;
    }
}
