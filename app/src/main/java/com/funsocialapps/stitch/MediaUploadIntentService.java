package com.funsocialapps.stitch;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaCodec;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.content.LocalBroadcastManager;

import java.io.File;
import java.io.IOException;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * helper methods.
 */
public class MediaUploadIntentService extends IntentService {

    public static final String ACTION_PHOTO = "com.funsocialapps.stitch.mediaupload.action.photo";
    public static final String ACTION_VIDEO = "com.funsocialapps.stitch.mediaupload.action.video";

    private static final String INPUT_URI = "com.funsocialapps.stitch.videoupload.input_uri";
    private static final String EVENT_ID = "com.funsocialapps.stitch.videoupload.event_id";

    public static final String ACTION_BROADCAST = "com.funsocialapps.stitch.videoupload.broadcast";
    public static final String ORIGINAL_ACTION = "com.funsocialapps.stitch.videoupload.original_action";
    public static final String STATUS = "com.funsocialapps.stitch.videoupload.status";

    /**
     * Starts this service to perform video upload task with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    public static void startPhotoUpload(Context context, String eventId, Uri photo) {
        L.debug("Starting photo upload...");
        Intent intent = new Intent(context, MediaUploadIntentService.class);
        intent.setAction(ACTION_PHOTO);
        intent.putExtra(INPUT_URI, photo);
        intent.putExtra(EVENT_ID, eventId);
        context.startService(intent);
    }

    /**
     * Starts this service to perform video upload task with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    public static void startVideoUpload(Context context, String eventId, Uri video) {
        L.debug("Starting video upload...");
        Intent intent = new Intent(context, MediaUploadIntentService.class);
        intent.setAction(ACTION_VIDEO);
        intent.putExtra(INPUT_URI, video);
        intent.putExtra(EVENT_ID, eventId);
        context.startService(intent);
    }

    public static void startMediaUpload(Context context, String eventId, Uri mediaURI) {
        String type = context.getContentResolver().getType(mediaURI);
        File file = new File(mediaURI.getPath());
        if (WebAPI.ContentType.isVideo(type)) {
            startVideoUpload(context, eventId, mediaURI);
        } else {
            startPhotoUpload(context, eventId, mediaURI);
        }
    }

    public MediaUploadIntentService() {
        super("MediaUploadIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        L.debug("Handling media intent...");

        if (intent == null) return;

        final Uri inputUri = (Uri)intent.getParcelableExtra(INPUT_URI);

        boolean status = false;
        String action = intent.getAction();
        String eventId = intent.getStringExtra(EVENT_ID);

        if (ACTION_PHOTO.equals(action)) {
            status = handlePhotoUpload(eventId, inputUri);
        } else if (ACTION_VIDEO.equals(action)) {
            status = handleVideoUpload(eventId, inputUri);
        }

        L.debug("MediaUploadIntentService responding with status " + status);

        Intent localIntent = new Intent(ACTION_BROADCAST);
        localIntent.putExtra(STATUS, status);
        localIntent.putExtra(ORIGINAL_ACTION, action);
        LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
    }

    /**
     * Handle phot upload in the provided background thread with the provided
     * parameters.
     */
    private boolean handlePhotoUpload(String eventId, Uri photo) {

        L.debug("MediaUploadIntentService handling photo upload for " + eventId + ", photo " + photo);

        try {
            WebAPI.postPhoto(this, eventId, photo, getCacheDir());
        } catch(Exception e) {
            L.error("Error uploading photo " + photo + ". ", e);
            return false;
        }

        return true;
    }

    /**
     * Handle video upload in the provided background thread with the provided
     * parameters.
     */
    private boolean handleVideoUpload(String eventId, Uri video) {

        L.debug("MediaUploadIntentService handling video upload for " + eventId + ", video " + video);

        try {
            File transcodedVideo = File.createTempFile("transcode", null, getCacheDir());
            try {
                L.debug("Transcoding video...");
                Transcoder.transcodeVideo(this, video, transcodedVideo);
                L.debug("Uploading transcoded video...");
                WebAPI.postVideo(this, eventId, Uri.fromFile(transcodedVideo), getCacheDir());
                L.debug("Video upload complete.");
            } finally {
                transcodedVideo.delete();
            }

        } catch(Exception e) {
            L.error("Error transcoding or uploading video " + video + ". ", e);

            if (Build.VERSION.SDK_INT >= 21) {
                if (e instanceof MediaCodec.CodecException) {
                    MediaCodec.CodecException ce = (MediaCodec.CodecException) e;
                    L.error("Codec Exception Diagnostics: " + ce.getDiagnosticInfo() + ". Is transient? " + ce.isTransient() + ". Is recoverable? " + ce.isRecoverable());
                }
            }

            return false;
        }

        return true;
    }
}
