package com.funsocialapps.stitch;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.Date;


public class AddEventActivity extends AppCompatActivity {

    Location location;

    static final String LOCATION_EXTRA = "location";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_event);

        location = (Location)getIntent().getParcelableExtra(LOCATION_EXTRA);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_event, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_save) {
            saveEvent();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void saveEvent() {
        EditText eventNameEditText = (EditText)findViewById(R.id.event_name);
        final String eventName = eventNameEditText.getText().toString().trim();
        final String id = WebAPI.getUniqueId();
        final Activity activity = this;

        if (eventName.length() == 0) {
            Toast.makeText(this, R.string.empty_event_name, Toast.LENGTH_SHORT).show();
            return;
        }

        final Runnable completion = new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent();
                intent.putExtra("event", new Event(eventName, id));
                setResult(RESULT_OK, intent);
                finish();
            }
        };

        final Runnable failedCompletion = new Runnable() {
            @Override
            public void run() {
                Toast.makeText(activity, R.string.event_creation_failed, Toast.LENGTH_SHORT).show();
            }
        };

        final Handler handler = new Handler();

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        String nowString = RFC3339.getDateFormat().format(new Date());
                        L.debug("nowString is " + nowString);

                        JSONObject obj = new JSONObject();
                        obj.put("name", eventName);
                        obj.put("id", id);
                        obj.put("time", nowString);
                        obj.put("latitude", location.getLatitude());
                        obj.put("longitude", location.getLongitude());
                        obj.put("anyone_can_post", true);
                        obj.put("discover_by_proximity", true);

                        int response = WebAPI.putJSONObject(WebAPI.getEventURL(id), obj);
                        L.debug("Response to create event was " + response);
                        if (response == HttpURLConnection.HTTP_CREATED) {
                            L.debug("Response OK");
                            handler.post(completion);
                        } else {
                            throw new Exception("Response code I didn't expect: " + response);
                        }
                    } catch (Exception e) {
                        L.error("Couldn't create event.", e);
                        handler.post(failedCompletion);
                    }
                }
            }).start();

    }
}
