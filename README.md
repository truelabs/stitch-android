Stitch Android
==============
Stitch client for Android. To build, install Android Studio.

To get command line tools that come with Android Studio into your PATH, run:

    ANDROID_SDK=$HOME/Library/Android/sdk
    PATH=$PATH:$ANDROID_SDK/tools
    PATH=$PATH:$ANDROID_SDK/platform-tools

